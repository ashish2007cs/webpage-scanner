package com.assignment.qualys.model;

import java.util.Date;

public class WebpageScannerModel {
	
	private String id;
	private String url;
	private String redirectionUrl;
	private Date submissionDate;
	private String websiteTitle;
	private String websiteBodyContent;
	private String imagesCount;
	private String linksCount;
	private Integer status;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getRedirectionUrl() {
		return redirectionUrl;
	}
	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}
	public Date getSubmissionDate() {
		return submissionDate;
	}
	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}
	public String getWebsiteTitle() {
		return websiteTitle;
	}
	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}
	public String getWebsiteBodyContent() {
		return websiteBodyContent;
	}
	public void setWebsiteBodyContent(String websiteBodyContent) {
		this.websiteBodyContent = websiteBodyContent;
	}
	public String getImagesCount() {
		return imagesCount;
	}
	public void setImagesCount(String imagesCount) {
		this.imagesCount = imagesCount;
	}
	public String getLinksCount() {
		return linksCount;
	}
	public void setLinksCount(String linksCount) {
		this.linksCount = linksCount;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

}
