package com.assignment.qualys.model;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Ashish Mishra
 */
public class WebpageScannerCriteria {

	@NotBlank(message = "hostname field  can't empty!")
	String hostname;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

}
