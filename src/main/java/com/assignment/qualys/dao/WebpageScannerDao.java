/**
 * 
 */
package com.assignment.qualys.dao;

import java.util.Date;
import java.util.List;

import com.assignment.qualys.entities.WebpageScannerEntity;

/**
 * @author ashishm
 *
 */
public interface WebpageScannerDao {

	List<WebpageScannerEntity> featchRecentUrlsByLimit(int limit);


	void insertURLProcessedData(WebpageScannerEntity webpageScannerentity);

	WebpageScannerEntity fetchReportById(Integer id);

	List<WebpageScannerEntity> urlsListByFilter(Integer limit, Date startDate, Date endDate);

}
