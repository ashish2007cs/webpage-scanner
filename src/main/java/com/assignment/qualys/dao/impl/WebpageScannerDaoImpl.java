package com.assignment.qualys.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.assignment.qualys.dao.WebpageScannerDao;
import com.assignment.qualys.entities.WebpageScannerEntity;


@Transactional
@Repository
public class WebpageScannerDaoImpl implements WebpageScannerDao{
	private static final Logger logger = LoggerFactory.getLogger(WebpageScannerDaoImpl.class);
	
	
	@PersistenceContext	
	private EntityManager entityManager;

	@Override
	public List<WebpageScannerEntity> featchRecentUrlsByLimit(int limit) {
		String sql = "FROM WebpageScannerEntity ws order by ws.submissionDate DESC";
		List<WebpageScannerEntity>  resultList = entityManager.createQuery(sql).setMaxResults(limit).getResultList();
		return resultList;
	}


	@Override
	public void insertURLProcessedData(WebpageScannerEntity webpageScannerentity) {
		try {
			entityManager.persist(webpageScannerentity);
		} catch (Exception e) {
			logger.error("got exception inside the insertURLProcessedData  "+e.getMessage());
		}
		
		
	}

	@Override
	public WebpageScannerEntity fetchReportById(Integer id) {
		String hql = "FROM WebpageScannerEntity ws where ws.id =:id";
		Query query = entityManager.createQuery(hql).setParameter("id",id);
		List<WebpageScannerEntity> results = query.getResultList();
		
		if(results !=null && results.size()>0) {
			return results.get(0);
		}
		return null;
	}

	@Override
	public List<WebpageScannerEntity> urlsListByFilter(Integer limit, Date startDate, Date endDate) {
		String hql = "SELECT ws.url from WebpageScannerEntity ws where ws.submissionDate between :startDate AND :endDate";
		Query query = entityManager.createQuery(hql).
				setParameter("startDate", startDate).
				setParameter("endDate", endDate).
				setMaxResults(limit);
		
		return query.getResultList();
	}

}
