package com.assignment.qualys.entities;

import static javax.persistence.GenerationType.IDENTITY;

// Generated 26 Jan, 2018 11:55:49 AM by Hibernate Tools 5.2.6.Final

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/*import lombok.Getter;
import lombok.Setter;
import lombok.ToString;*/

/**
 * Report generated by hbm2java
 */
/*@Getter
@Setter
@ToString*/
@Entity
@Table(name = "report", catalog = "webpage_scanner")
public class WebpageScannerEntity implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String url;
	private String ip;
	private String redirectionUrl;
	private Date submissionDate;
	private String websiteTitle;
	private String websiteBodyContent;
	private Integer imagesCount;
	private Integer linksCount;
	private Integer status;

	public WebpageScannerEntity() {
	}

	public WebpageScannerEntity(String url, Date submissionDate) {
		this.url = url;
		this.submissionDate = submissionDate;
	}

	public WebpageScannerEntity(String url, String ip, String redirectionUrl, Date submissionDate, String websiteTitle,
			String websiteBodyContent, Integer imagesCount, Integer linksCount, Integer status) {
		this.url = url;
		this.ip = ip;
		this.redirectionUrl = redirectionUrl;
		this.submissionDate = submissionDate;
		this.websiteTitle = websiteTitle;
		this.websiteBodyContent = websiteBodyContent;
		this.imagesCount = imagesCount;
		this.linksCount = linksCount;
		this.status = status;
	}

	@Override
	public String toString() {
		return "WebpageScannerEntity [id=" + id + ", url=" + url + ", ip=" + ip + ", redirectionUrl=" + redirectionUrl
				+ ", submissionDate=" + submissionDate + ", websiteTitle=" + websiteTitle + ", websiteBodyContent="
				+ websiteBodyContent + ", imagesCount=" + imagesCount + ", linksCount=" + linksCount + ", status="
				+ status + "]";
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)

	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "url", nullable = false, length = 100)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name = "ip")
	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "redirection_url", length = 100)
	public String getRedirectionUrl() {
		return this.redirectionUrl;
	}

	public void setRedirectionUrl(String redirectionUrl) {
		this.redirectionUrl = redirectionUrl;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "submission_date", nullable = true, length = 19)
	public Date getSubmissionDate() {
		return this.submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	@Column(name = "website_title", length = 100)
	public String getWebsiteTitle() {
		return this.websiteTitle;
	}

	public void setWebsiteTitle(String websiteTitle) {
		this.websiteTitle = websiteTitle;
	}

	@Column(name = "website_body_content" ,columnDefinition="longtext")
	public String getWebsiteBodyContent() {
		return this.websiteBodyContent;
	}

	public void setWebsiteBodyContent(String websiteBodyContent) {
		this.websiteBodyContent = websiteBodyContent;
	}

	@Column(name = "images_count")
	public Integer getImagesCount() {
		return this.imagesCount;
	}

	public void setImagesCount(Integer imagesCount) {
		this.imagesCount = imagesCount;
	}

	@Column(name = "links_count")
	public Integer getLinksCount() {
		return this.linksCount;
	}

	public void setLinksCount(Integer linksCount) {
		this.linksCount = linksCount;
	}

	@Column(name = "status")
	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
