package com.assignment.qualys.config;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import com.assignment.qualys.util.WebpageScannerConstant;

/**
 * Ashish Mishra  
 */
public class ThreadPoolUtil {

	public static CustomThreadPool getThreadPool(String fetchRecentUrlsByLimit) {
		int threadPoolSize = WebpageScannerConstant.PRODUCT_THREAD_POOL_SIZE_DEFAULT_VALUE;
		
		int threadPoolQueueSize = WebpageScannerConstant.PRODUCT_THREAD_POOL_QUEUE_SIZE_DEFAULT_VALUE;
		int ttlValue = WebpageScannerConstant.PRODUCT_THREAD_POOL_DEFAULT_TTL_VALUE_IN_SECONDS;
		
		
		
		CustomThreadPool customThreadPool = new CustomThreadPool(threadPoolSize, threadPoolSize,
				  ttlValue, TimeUnit.SECONDS,
				  new ArrayBlockingQueue(threadPoolQueueSize));
		
		customThreadPool.allowCoreThreadTimeOut(true);
		customThreadPool.setPoolName("fetchRecentUrlsByLimit thread pool");
		
		return customThreadPool;
	}


}
