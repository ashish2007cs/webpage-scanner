package com.assignment.qualys.config;

import java.util.List;

import org.springframework.web.context.request.async.DeferredResult;

/**
 * Ashish Mishra  
 */
public interface CustomRunnable<T> extends Runnable{
	
	/**
	 * Gets deferred result object.
	 *
	 * @return the deferred result object
	 */
	DeferredResult<T> getDeferredResultObject();

}
