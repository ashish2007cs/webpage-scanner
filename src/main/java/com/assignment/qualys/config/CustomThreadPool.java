package com.assignment.qualys.config;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Ashish Mishra  
 */
public class CustomThreadPool extends ThreadPoolExecutor{
	
	private String poolName;

	public String getPoolName() {
		return poolName;
	}

	public CustomThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	public void setPoolName(String string) {
		this.poolName  = string;
		
	}

}
