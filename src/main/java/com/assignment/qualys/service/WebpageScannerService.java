package com.assignment.qualys.service;

import java.util.Date;
import java.util.List;

import com.assignment.qualys.entities.WebpageScannerEntity;
import com.assignment.qualys.model.WebpageScannerModel;


public interface WebpageScannerService {
	
	List<WebpageScannerEntity> featchRecentUrlsByLimit(int limit);
	

	WebpageScannerEntity processHostName(String hostname);
	
	WebpageScannerEntity fetchReportById(Integer id);

	List<WebpageScannerEntity> urlsListByFilter(Integer limit, Date startDate, Date endDate);
	
	
	

}
