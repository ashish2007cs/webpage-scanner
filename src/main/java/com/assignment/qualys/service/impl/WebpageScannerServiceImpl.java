package com.assignment.qualys.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.assignment.qualys.dao.WebpageScannerDao;
import com.assignment.qualys.entities.WebpageScannerEntity;
import com.assignment.qualys.service.WebpageScannerService;

@Service
public class WebpageScannerServiceImpl implements WebpageScannerService {
	
	private static final Logger logger = LoggerFactory.getLogger(WebpageScannerServiceImpl.class);

	@Autowired
	WebpageScannerDao webpageScannerDao;
	
	AtomicInteger flag = new AtomicInteger(1);
	

	
	@Override
	public List<WebpageScannerEntity> featchRecentUrlsByLimit(int limit) {
		List<WebpageScannerEntity> featchRecentUrlsByLimit = webpageScannerDao.featchRecentUrlsByLimit(limit);
		return featchRecentUrlsByLimit;
	}


	@Override
	public WebpageScannerEntity processHostName(String hostname) {
		WebpageScannerEntity webpageScannerentity = extractHtmlInfoFromHostName(hostname);
		webpageScannerDao.insertURLProcessedData(webpageScannerentity);
		return webpageScannerentity;
	}

	private WebpageScannerEntity extractHtmlInfoFromHostName(String hostname) {
		String errorMessage= "NA";
		try {
			if(!urlValidator(hostname)){
				return getentityObject(hostname , "URL is not Valid ");
			} 
			// The URL address of the page to open.
			URL url = new URL(hostname);

			// identify redirection URL
			String rURL = identifyRedirectionURL(url);
			String ip = extractIpAddress(url.getHost());
			String content = readFromHostWebPage(url);
			int imagesCount = getImageCount(content);
			int linkCount = getHyperLinkCount(content);
			String title = validateAndReplaceText(getHtmlTitle(content)); ;
			String bodyContent = validateAndReplaceText(getBodyContent(content)); 
			return new WebpageScannerEntity(hostname, ip, rURL, new Date(), title, bodyContent, imagesCount, linkCount, flag.get());
		}
			
			catch (MalformedURLException e) {
			errorMessage = "Exception:"+e.getMessage();
			logger.error("got exception for URL: {}, exception is {} "+hostname, e.getMessage());
		}
		return getentityObject(hostname, errorMessage);
		

		
		
	}


	private WebpageScannerEntity getentityObject(String hostname , String message) {
		return new WebpageScannerEntity(hostname, "NA", "NA", new Date(), "NA", message, 0, 0, 1);
	}

	 public static boolean urlValidator(String url)
	    {
	        UrlValidator defaultValidator = new UrlValidator();
	        return defaultValidator.isValid(url);
	    }
	 
	 
	private String validateAndReplaceText(String content) {
		if(StringUtils.hasText(content)) {
			logger.info("inside verifyBodyContents method  " , content);
			return content.replace("\n", "").replace("\r", "").replaceAll("^\\s+|\\s+$|\\s*(\n)\\s*|(\\s)\\s*", "$1$2")
				     .replace("\t"," ").replaceAll("[^\\p{L}\\p{Nd}\\s]+", "");
		}
		
	     
		return null;
	}

	private String getBodyContent(String content) {
		Pattern p = Pattern.compile("<body>(.*?)</body>", Pattern.DOTALL); 
		Matcher m = p.matcher(content);
		while (m.find()) {
			content = m.group(1);
		}
		
		// Remove style tags & inclusive content
		Pattern style = Pattern.compile("<style.*?>.*?</style>");
		Matcher mstyle = style.matcher(content);
		while (mstyle.find())
			content = mstyle.replaceAll("");

		// Remove script tags & inclusive content
		Pattern script = Pattern.compile("<script.*?>.*?</script>");
		Matcher mscript = script.matcher(content);
		while (mscript.find())
			content = mscript.replaceAll("");
		
		//System.out.println("content "+content);
		// Remove primary HTML tags
		Pattern tag = Pattern.compile("<.*?>");
		Matcher mtag = tag.matcher(content);
		while (mtag.find())
			content = mtag.replaceAll("");

		// Remove comment tags & inclusive content
		Pattern comment = Pattern.compile("<!--.*?-->");
		Matcher mcomment = comment.matcher(content);
		while (mcomment.find())
			content = mcomment.replaceAll("");

		// Remove special characters, such as &nbsp;
		Pattern sChar = Pattern.compile("&.*?;");
		Matcher msChar = sChar.matcher(content);
		while (msChar.find())
			content = msChar.replaceAll("");

		// Remove the tab characters. Replace with new line characters.
		Pattern nLineChar = Pattern.compile("\t+");
		Matcher mnLine = nLineChar.matcher(content);
		while (mnLine.find())
			content = mnLine.replaceAll("\n");
		
		
		return content;
		
	}

	private String getHtmlTitle(String content) {
		Pattern p = Pattern.compile("<head>.*?<title>(.*?)</title>.*?</head>", Pattern.DOTALL);
		String title = "NA";
		Matcher m = p.matcher(content);
		while (m.find()) {
			title = m.group(1);
		}
		return title;
	}

	private int getHyperLinkCount(String html) {
		int result = 0;
		Pattern patternTag = Pattern.compile("(?i)<a([^>]+)>(.+?)/a>");
		Matcher matcher = patternTag.matcher(html);
		while(matcher.find())
			result++;
		
		

		return result;
		}

	private int getImageCount(String html) {
		
		int result = 0;
		Pattern patternTag = Pattern.compile("(?s)(<img.*?)(src\\s*?=\\s*?(?:\"|').*?(?:\"|'))");
		Matcher matcher = patternTag.matcher(html);
		while(matcher.find())
			result++;
		
		return result;
		}

	private String  readFromHostWebPage(URL url) {
		String sourceLine;
		String content = "";
		BufferedReader source = null;
		InputStreamReader pageInput = null;
		
		// Open the address and create a BufferedReader with the source code.
		try {
			
			
			 HttpURLConnection httpcon = (HttpURLConnection) url.openConnection(); 
			 httpcon.addRequestProperty("User-Agent", "Mozilla/4.76"); 
			 pageInput = new InputStreamReader(httpcon.getInputStream());
			 source = new BufferedReader(pageInput);
			
			// Append each new HTML line into one string. Add a tab character.
			while ((sourceLine = source.readLine()) != null)
				content += sourceLine + "\t";
			
			flag.set(0);
		} catch (IOException e) {
			content = "Exception:"+e.getMessage();
			logger.error("Could not read content for URL: {}, exception is {} "+url,e.getMessage());
		}
		finally {
			try {
				pageInput.close();
				source.close();
			} catch (IOException e) {
				content = content + "Exception:"+e.getMessage();
				logger.error("exception in closing the resource "+ content);
			}
		}
		
		return StringUtils.hasText(content) ? content : "NA";
		
	}

	private String identifyRedirectionURL(URL address) {
		HttpURLConnection conn;
		String rUrl ="NA";
		try {
			System.out.println("address is ... " + address);
			conn = (HttpURLConnection) address.openConnection();
			conn.setReadTimeout(5000);
			conn.addRequestProperty("Accept-Language", "en-US,en;q=0.8");
			conn.addRequestProperty("User-Agent", "Mozilla");
			conn.addRequestProperty("Referer", "google.com");


			boolean redirect = false;

			// normally, 3xx is redirect
			int status = conn.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK) {
				if (status == HttpURLConnection.HTTP_MOVED_TEMP
					|| status == HttpURLConnection.HTTP_MOVED_PERM
						|| status == HttpURLConnection.HTTP_SEE_OTHER)
				redirect = true;
			}
			if (redirect) {

				// get redirect url from "location" header field
				return conn.getHeaderField("Location");

			}
		} catch (IOException e) {
			rUrl = "Exception:"+e.getMessage();
			e.printStackTrace();
		}
		return rUrl;
		

		

		
	}

	private String extractIpAddress(String hostname) {
		String res = null;
		try {
			InetAddress ipaddress = InetAddress.getByName(hostname);
			flag.set(0);
			return ipaddress.getHostAddress();
		} catch (UnknownHostException e) {
			res = "Exception:"+e.getMessage();
			logger.error("Could not find IP address for {}, exception is {} "+hostname,e.getMessage());
		}
		return res;

	}

	@Override
	public WebpageScannerEntity fetchReportById(Integer id) {
		return webpageScannerDao.fetchReportById( id);
	}

	@Override
	public List<WebpageScannerEntity> urlsListByFilter(Integer limit, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return webpageScannerDao.urlsListByFilter(limit, startDate, endDate);
	}

}
