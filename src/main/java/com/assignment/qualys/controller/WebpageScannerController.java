package com.assignment.qualys.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Ashish Mishra  
 */
@Controller
public class WebpageScannerController {
	private static final Logger logger = LoggerFactory.getLogger(WebpageScannerController.class);
	
	@GetMapping("/")
	public String index() {
		logger.debug("Inside / method ");
		return "homepage";
	}

}
