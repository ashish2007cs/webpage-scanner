package com.assignment.qualys.controller.rest;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import com.assignment.qualys.config.CustomRunnable;
import com.assignment.qualys.config.CustomThreadPool;
import com.assignment.qualys.config.ThreadPoolUtil;
import com.assignment.qualys.entities.WebpageScannerEntity;
import com.assignment.qualys.model.WebpageScannerCriteria;
import com.assignment.qualys.service.WebpageScannerService;

import io.swagger.annotations.ApiOperation;

/**
 * Ashish Mishra
 */

@RestController
public class WebpageScannerRestController {

	private static final Logger logger = LoggerFactory.getLogger(WebpageScannerRestController.class);

	@Autowired
	WebpageScannerService webpageScannerService;

	@GetMapping("/fetchRecentUrlsByLimit")
	public @ResponseBody DeferredResult<List<?>> fetchRecentUrlsDataByLimit(
			@RequestParam(value = "limit", defaultValue = "10") Integer limit) {
		DeferredResult<List<?>> deferredResult = new DeferredResult<>();
		logger.info("inside fetchRecentUrlsDataByLimit method  ");
		CustomThreadPool customThreadPool = ThreadPoolUtil.getThreadPool("fetchRecentUrlsByLimit");

		customThreadPool.submit(new NioWebpageScannerRestController(limit, deferredResult));

		// Return deferredResult
		return deferredResult;
	}
	
	

	public ResponseEntity<List<?>> executeFetchRecentUrlsDataByLimit(Integer limit, DeferredResult<List<?>> deferredResult) {
		logger.info("inside executeFetchRecentUrlsDataByLimit method ");
		List<WebpageScannerEntity> recentUrlsList = webpageScannerService.featchRecentUrlsByLimit(limit);
		
		//Add response to deferredResult
	     deferredResult.setResult(recentUrlsList);
	     
		return ResponseEntity.ok(recentUrlsList);
	}
	
	

	@ApiOperation(value = "return response in Json format. here NA keywaord means not available in webpage and status = 0 means sucess othewise failed.")
	@PostMapping("/submit/url")
	public DeferredResult<?>  submitHostName(@Valid @RequestBody WebpageScannerCriteria form) {
		logger.info("inside submitHostName method ");
		DeferredResult<WebpageScannerEntity> deferredResult = new DeferredResult<>();

		CustomThreadPool customThreadPool = ThreadPoolUtil.getThreadPool("submitHostName");

		customThreadPool.submit(new NioSubmitHostNameRestController(form.getHostname(), deferredResult));

		// Return deferredResult
		return deferredResult;
		

		
	}
	
	public ResponseEntity<WebpageScannerEntity> executeSubmitHostName(String hostname2, DeferredResult<WebpageScannerEntity> deferredResult) {
		
		logger.info("inside executeSubmitHostName method ");
		
		WebpageScannerEntity processHostNameResponse = webpageScannerService.processHostName(hostname2);

		//Add response to deferredResult
	     deferredResult.setResult(processHostNameResponse);
	     
		return ResponseEntity.ok(processHostNameResponse);
		
	}
	

	@GetMapping("/fetchReportById")
	public ResponseEntity<?> fetchReportById(@RequestParam(value = "id") Integer id) {

		WebpageScannerEntity data = webpageScannerService.fetchReportById(id);

		return ResponseEntity.ok(data);

	}
	
	

	@ApiOperation(value = "set value of limit to fetch no of urls. , send start and end date in timestamp format only e.g 2018-01-26")
	@GetMapping("/list")
	public ResponseEntity<List<?>> list(@RequestParam(value = "limit") Integer limit,
			@RequestParam(value = "startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@RequestParam(value = "endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) {

		List<WebpageScannerEntity> recentUrlsList = webpageScannerService.urlsListByFilter(limit, startDate, endDate);

		return ResponseEntity.ok(recentUrlsList);
	}
	
	

	public class NioWebpageScannerRestController implements CustomRunnable<List<?>> {
		private Integer limit;
		DeferredResult<List<?>> deferredResult;

		public NioWebpageScannerRestController(Integer limit, DeferredResult<List<?>> deferredResult) {
			this.limit = limit;
			this.deferredResult = deferredResult;
		}

		@Override
		public void run() {
			executeFetchRecentUrlsDataByLimit(limit, deferredResult);
		}

		@Override
		public DeferredResult<List<?>> getDeferredResultObject() {
			return deferredResult;
		}

	}
	
	public class NioSubmitHostNameRestController implements CustomRunnable<WebpageScannerEntity> {
		String hostname;
		DeferredResult<WebpageScannerEntity> deferredResult;

		public NioSubmitHostNameRestController(String hostname, DeferredResult<WebpageScannerEntity> deferredResult) {
			this.hostname = hostname;
			this.deferredResult = deferredResult;
		}

		@Override
		public void run() {
			executeSubmitHostName(hostname, deferredResult);
		}

		

		@Override
		public DeferredResult<WebpageScannerEntity> getDeferredResultObject() {
			return deferredResult;
		}

	}


	
}
