package com.assignment.qualys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationScannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationScannerApplication.class, args);
		
	}
	
	
}
