package com.assignment.qualys.util;

/**
 * Ashish Mishra  
 */
public class WebpageScannerConstant {

	public static final int PRODUCT_THREAD_POOL_SIZE_DEFAULT_VALUE = 1000;
	public static final int PRODUCT_THREAD_POOL_QUEUE_SIZE_DEFAULT_VALUE = 1000;
	public static final int PRODUCT_THREAD_POOL_DEFAULT_TTL_VALUE_IN_SECONDS = 60;

}
