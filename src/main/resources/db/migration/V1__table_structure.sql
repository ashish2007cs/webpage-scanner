
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(100) NOT NULL,
  `ip` varchar(100) DEFAULT NULL,
  `redirection_url` varchar(100) DEFAULT NULL,
  `submission_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `website_title` varchar(100) DEFAULT NULL,
  `website_body_content` longtext,
  `images_count` int(11) DEFAULT '0',
  `links_count` int(11) DEFAULT '0',
  `status` int(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=latin1;
