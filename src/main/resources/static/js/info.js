
var limit = 10;

$(document).ready(function() {
	$('.error').hide();
	$('#report_page').hide();

	jQuery.support.cors = true;
	init();

	$("#id_sumbitForm").submit(function(event) {
		//stop submit the form, we will post it manually.
		event.preventDefault();

		$('.error').hide();
		var name = $("#txt_host").val();
		if (name == "") {
			$("label#host_error").show();
			$("#txt_host").focus();
			return false;
		}

		if (name.length > 100) {
			$("label#host_txt_error").show();
			$("#txt_host").focus();
			return false;
		}

		submit_host_form();

	});

	$("#firstPageTable").on('click', 'a', function() {
		//var id = $(this).find("td:nth-child(6)").text()
		var currentRow = $(this).closest("tr");
		var id = currentRow.find("td.id").text();
		fetchReportDataById(id);

	});

})

function submit_host_form() {

	var form = {}
	form["hostname"] = $("#txt_host").val();

	//$("#submit").prop("disabled", true);

	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "/submit/url",
		data : JSON.stringify(form),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(data) {
			console.log(data);
			if (data.length != 0) {
				populateData(data);
			}
		},
		error : function(e) {


			console.log("ERROR : ", e);

		}
	});
}

function validate_submit_host_form() {

	$('.error').hide();
	var name = $("#txt_host").val();
	if (name == "") {
		$("label#name_error").show();
		$("input#name").focus();
		return false;
	}

}

function init() {
	$.ajax({
		url : "/fetchRecentUrlsByLimit?limit=" + limit,
		success : function(response) {
			if (response.length != 0) {
				var tabledata = "";
				$.each(response, function(i, data) {
					var status = data.status == 0 ? "success" : "failed";
					var row = $("<tr id ='tr_row'><td>" + "<a href='#'>"
							+ data.url + "</a></td><td>" + status
							+ "</td><td>" + data.ip + "</td><td>"
							+ data.websiteTitle + "</td><td>"
							+ new Date(data.submissionDate)
							+ "</td><td class='id'style='display:none;'>"
							+ data.id + "</td></tr>");
					$("#requestLog").append(row);

				});

			}

		},
		error : function(e) {


			console.log("ERROR : ", e);

		}
	});
}

function fetchReportDataById(id) {

	var data = {}
	data["id"] = id;

	//$("#submit").prop("disabled", true);

	$.ajax({
		url : "/fetchReportById?id="+id,
		success : function(data) {
			console.log(data);
			if (data.length != 0) {
				populateData(data);
			}

		},
		error : function(e) {
			console.log("ERROR : ", e);

		}
	});

}

function populateData(data){

	var tabledata = "";

	var row = $("<tr><td>" + data.url + "</a>" + "</td><td>"
			+ data.redirectionUrl + "</a>" + "</td><td>"
			+ new Date(data.submissionDate) + "</td><td >"
			+ data.websiteTitle + "</td><td>"
			+ data.websiteBodyContent + "</td><td>"
			+ data.imagesCount + "</td><td>" + data.linksCount
			+ "</td></tr>");
	$("#reportLog").append(row);
	$('.container').hide();

	$('#report_page').show();


	
}
