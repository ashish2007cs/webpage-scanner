package com.assignment.qualys;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.assignment.qualys.controller.rest.WebpageScannerRestController;
import com.assignment.qualys.entities.WebpageScannerEntity;
import com.assignment.qualys.service.WebpageScannerService;

/**
 * Ashish Mishra  
 */

@RunWith(SpringRunner.class)
@WebMvcTest(WebpageScannerRestController.class)
public class WebpageScannerRestControllerTest {
	/*
	@Autowired
	WebpageScannerRestController webpageScannerRestController;*/
	
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private  WebpageScannerService webpageScannerService;
	
	List<WebpageScannerEntity> list;
	
	@Test
	public void contextLoads() {
		
		WebpageScannerEntity mockEntity = new WebpageScannerEntity("www.google.com", "127.0.0.1","NA",new Date(),"NA","NA", 0, 0, 0);
		WebpageScannerEntity mockEntity1 = new WebpageScannerEntity("www.google.com", "127.0.0.1","NA",new Date(),"NA","NA", 0, 0, 0);
		list = new ArrayList<WebpageScannerEntity>();
		list.add(mockEntity);
		list.add(mockEntity1);
		
	}
	
	
	@Test
	public void fetchRecentUrlsDataByLimit() throws Exception {
		WebpageScannerEntity mockEntity = new WebpageScannerEntity("www.google.com", "127.0.0.1","NA",new Date(),"NA","NA", 0, 0, 0);
		WebpageScannerEntity mockEntity1 = new WebpageScannerEntity("www.google.com", "127.0.0.1","NA",new Date(),"NA","NA", 0, 0, 0);
		
		
		List<WebpageScannerEntity> list = Arrays.asList(mockEntity,mockEntity1);
		
		Mockito.when(
				webpageScannerService.featchRecentUrlsByLimit(Mockito.anyInt())).thenReturn(list);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/fetchRecentUrlsByLimit");

		List responseList =  (List) mockMvc.perform(requestBuilder).andReturn().getAsyncResult();
		assertThat(responseList.get(0)).isEqualTo(mockEntity);
		assertThat(responseList.get(0)).isEqualTo(mockEntity);
		
		
	}
	
	
	@Test
	public void test_fetchReportById_test() throws Exception {
		
		WebpageScannerEntity mockEntity = new WebpageScannerEntity("www.google.com", "127.0.0.1","NA",new Date(),"NA","NA", 0, 0, 0);
		
		Mockito.when(
				webpageScannerService.fetchReportById(Mockito.anyInt())).thenReturn(mockEntity);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/fetchReportById").param("id", "10");
		
		String contentAsString = mockMvc.perform(requestBuilder).andReturn().getResponse().getContentAsString();
		assertThat(contentAsString.contains("\"url\":\"www.google.com\""));
	}
	
	
	
	
	


}
