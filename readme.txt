project configuration
1. import project in eclipse or other IDE.
2. open application.properties and chenge DB details.

DB setup - 
2. I have used flyway for DB schema migration and versioning .(schema_version table having the all entries).
3. Integrated with spring boot So table and schema wouldbe automaticly generated .
4. DB schema and table would be created , in case if we got any exception related to flyway schema(DB) migration. 
	a. please run mvn flyway:repair command(first change DB details in pom.xml )
	b. or delete the whole schema from db manually 
	above process can be automate but need some efforts.
	
Build - 
5. run maven install(clean install)

Run application 
6. we can run application with any one approach
	a. in eclipse run as Spring boot app
	b. in command line java -jar target/webpage_scanner-0.0.1-SNAPSHOT.jar
	
7. hit  http://<Ip address>:port on browser  e.g (http://localhost:8080/)

8. in home page , have one input field with button to submit and populate the result  data in report page in sync(Since this was not clear for me to only need to save url and save results in DB or need to display result in same time in sync. if only need to submit the url then we can use async method to store and process the URL in db with satus(e.g. Rabbit MQ, JMS , KAFKA))

9. top 10 url (by date )) will be dispalyed in home page  After clicking on URL link , link details populated in report page with Home link to go in previous(home page).
 
10. Rest API’s -  for interactive API  hit http://<Ip address>:por/swagger-ui.html on browser e.g. (http://localhost:8080/swagger-ui.html)
	a. /list api will take  3 params  (limit , startDate and enDate) . limit is integer type and Date needs to be in YYYY-MM-DD format
	b. /submit/url/ will take a form with hostname param .
	c. Deliberately Ignored the same url if someone is submitting the same , in scanning perspective  bevause webpage contents can be change every time.


11. made fetch recent last 10 url configurable so if in  case any  requirements changed then just change the value and can make it DB driven.

Performance 
1. written  NIO Rest controller to serve more request  and configued  thread pool to serve more request (kept 1000 thread pool size).
2, Configired DB connection Pool.
3. In order to make more scaable and serve 50000 request we can implement RAbbit MQ to serve request and wil process the request in background and indert in DB.(Sicne I need to display records just after processing the URL thats why implement Non blocking controller).
4. There  is many corner cases and scenario need to be handle but due to timing issue i have to ignore this .
5. Due to time crunch not able to write  test cases for all api.

12. Technologies 
	1.Spring Boot
	2. Hibernate
	3. My sql 
	4. Flyay
	5. Web sevice Rest
	6. Swagger
	7. Mockito unit test case
	8. Maven
	9. Html, bootstrap, Jquery and AJax call
	10 tomcat

	